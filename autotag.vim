" Words
3,$s/\w\+/<w type="###">&<\/w>/ge
" Apostrophes
3,$s/n<\/w>\('\|’\)<w type="###">t/<\/w><w type="###">n't/ge
3,$s/<\/w>'<w type="###">\(s\|m\|re\|d\|ll\|ve\)/<\/w><w type="###">'\1/ge
3,$s/'<w type="###">\(t\)\(is\|was\|would\|will\)\>/<w type="PNP">'\1<\/w><w type="###">\2/ge
3,$s/<w type="###">t<\/w>'<w/<w type="AT0">t'<\/w><w/ge
3,$s/<w type="###">d<\/w>'<w/<w type="PRF">d'<\/w><w/ge
" Hyphens and Dashes
3,$s/<\/w>\(-\|\/\)<w type="###">/\1/ge
3,$s/ \(–\|–\|—\|-\) / <c type="PUN">\1<\/c> /ge
3,$s/>—</><c type="PUN">—<\/c></ge
" Punctuation
3,$s/\.\.\./…/ge
3,$s/\(\.\|…\|\,\|!\|?\|\:\|\;\)/<c type="PUN">\1<\/c>/ge
3,$s/\((\|\[\)/<c type="PUL">\1<\/c>/ge
3,$s/\()\|\]\)/<c type="PUR">\1<\/c>/ge
3,$s/ \('\|"\)</ <c type="PUQ">\1<\/c></ge
3,$s/^\('\|"\)</<c type="PUQ">\1<\/c></ge
3,$s/\('\|"\) </<c type="PUQ">\1<\/c> </ge
3,$s/\('\|"\)<c/<c type="PUQ">\1<\/c><c/ge
3,$s/<\/c>\('\|"\)/<\/c><c type="PUQ">\1<\/c>/ge
" Search for untagged apostrophes, just in case
/<\/w>'<w
